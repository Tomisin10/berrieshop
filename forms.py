#This is just a reference point for creating forms in the future
# from flask_wtf import Form
from wtforms import Form, TextField, TextAreaField, SubmitField, validators, ValidationError, StringField


class SignupForm(Form):
    name = StringField("Fullname", [validators.DataRequired("Please enter Your Name")])
    email = StringField("Email", [validators.DataRequired("Please enter your email address"),
                                  validators.Email("Please enter your email address")])
    password = StringField("Password", [validators.DataRequired("Password must be more than 6 characters")])
    # subject = TextField("Subject", [validators.DataRequired("Please enter a subject.")])
    # message = TextAreaField("Message", [validators.DataRequired("Please enter a message.")])
    submit = SubmitField("SUBMIT")


class LoginForm(Form):
    name = StringField("Name", [validators.DataRequired("Please enter Your Name")], id='name-form1-q')
    email = StringField("Email", [validators.DataRequired("Please enter your email address"),
                                validators.email("Please enter your email address")], id='email-form1-q')
    password = StringField("Password", [validators.DataRequired("Password must be more than 6 characters")],
                         id='password-form1-q')
    # subject = TextField("Subject", [validators.DataRequired("Please enter a subject.")])
    # message = TextAreaField("Message", [validators.DataRequired("Please enter a message.")])
    submit = SubmitField("SUBMIT")