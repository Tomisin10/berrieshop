import random
import time
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy import update
from database import Base, engine, db_session


class Customer(Base):
    __tablename__ = 'customers'

    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    email = Column(String(200), unique=True)
    birthday = Column(String())
    password = Column(String(100))
    address = Column(String())
    interests = Column(String())
    received_emails = Column(String())
    library = Column(String())
    cart = Column(String())
    store_credit = Column(Integer())
    spare_dict = Column(String())

    def __init__(self, name=None, email=None, password=None, address=None, phone='', birthday='', interests=[],
                 received_emails=[], library=[], cart=[], store_credit=0, spare_dict={}):

        self.id = random.randint(1, 1000000000000)#self.generate_id()
        self.name = name
        self.email = email
        self.password = password
        self.address = str(address)
        self.phone = phone
        self.birthday = birthday
        self.interests = str(interests)
        self.received_emails = str(received_emails)
        self.library = str(library)
        self.cart = str(cart)
        self.store_credit = store_credit
        self.spare_dict = str(spare_dict)


class Product(Base):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True)
    title = Column(String())
    store = Column(String())
    tags = Column(String())
    orders_customers = Column(String())
    orders_date = Column(String())
    orders_qty = Column(String())
    price = Column(Float())
    profit_margin = Column(Float())
    ext_app = Column(String())
    images = Column(String())
    series = Column(String())
    spare_dict = Column(String())

    def __init__(self, title=None, author=None, publisher=None, tags=[], orders_customers=[], orders_date=[],
                 orders_qty=[], price=0, version='', images=[], profit_margin=1.3, description=0, series=[],
                 spare_dict=[]):

        self.id = random.randint(1, 1000000000000)
        self.title = title
        self.author = author
        self.publisher = publisher
        self.tags = tags
        self.orders_customers = str(orders_customers)
        self.orders_date = str(orders_date)
        self.orders_qty = str(orders_qty)
        self.price = price
        self.profit_margin = str(profit_margin)
        self.version = version
        self.description = description
        self.images = str(images)
        self.series = str(series)
        self.spare_dict = str(spare_dict)


class Store(Base):
    __tablename__ = 'stores'

    id = Column(Integer, primary_key=True)
    name = Column(String())
    email_address = Column(String())
    city = Column(String())
    state = Column(String())
    ext_apps = Column(String())
    personalized = Column(String())
    stock = Column(String())
    spare_dict = Column(String())

    def __init__(self, name=None, email_address=None, city=None, state=None, ext_apps=[], personalized={}, stock=[],
                 spare_dict={}):
        self.id = random.randint(1, 1000000000000)
        self.name = name
        self.email_address = email_address
        self.city = city
        self.state = state
        self.ext_apps = str(ext_apps)
        self.personalized = str(personalized)
        self.stock = str(stock)
        self.spare_dict = str(spare_dict)


class Order(Base):
    __tablename__ = 'orders'

    id = Column(Integer, primary_key=True)
    name = Column(String())
    customer_fullname = Column(String())
    customer_email = Column(String())
    customer_address = Column(String())
    date = Column(String())
    books = Column(String())
    spare_dict = Column(String())

    def __init__(self, name=None, customer_id='', customer_fullname='', customer_email='', customer_address='',
                 books=[], spare_dict={}, promo_code=''):
        self.id = random.randint(1, 1000000000000)
        self.name = name
        self.customer_id = customer_id
        self.customer_fullname = customer_fullname
        self.customer_email = customer_email
        self.customer_address = customer_address
        self.promo_code = promo_code
        self.date = time.ctime()
        self.books = str(books)
        self.spare_dict = str(spare_dict)
