__about__ = "Portal for publishing companies/individuals to set up stalls, and sell at retail rates."
__title__ = "BooksNG"

fb_app_id = ''

import cgi, os, sys, json, time, random
import urllib2
import argparse
from flask import Flask
from flask import request, url_for, render_template, redirect
from flask import session
from database import *
from forms import *
from models import Customer, Product, Store, Order
from celery import Celery

app = Flask(__name__)

servers_ip, redis_port = '', ''

app.config.update(CELERY_BROKER_URL='redis://%s:%s' % (servers_ip, redis_port),
                  CELERY_RESULT_BACKEND='redis://%s:%s' % (servers_ip, redis_port))

celery = make_celery(app)


@celery.task()
def some_long_process(a, b):
    return a + b


def add_user_sample():
    customer = Customer(name='Tomisin Abiodun', email='decave12357@gmail.com', password='gooder',
                        address='9, Victor Ojelabi Avenue, Akinbo, Akute, Ogun State', phone='08107684553',
                        interests=['Management', 'Programming'])
    db_session.add(customer)
    db_session.commit()


def edit_user_sample(email):
    user_file = Customer.query.filter(Customer.email == email).first()  # .update({User.foo: User.foo + 1})
    user_file.password = 'new password'
    db_session.commit()


def get_user_file(email):
    return Customer.query.filter(Customer.email == email).first()


@app.route('/')
@app.route('/home')
def home():
    return 'Welcome to %s<br><i>%s</i><hr>' % (__title__, __about__)


@app.route('/book')
def book():
    pass


@app.route('/add_to_cart')
def add_to_cart():
    book_id = request.form.get('book_id')

    session['cart'].append(book_id)

    # so that user could access cart from other devices
    if 'user_email' in session:
        user_file = get_user_file(session['user_email'])
        user_cart = eval(user_file.cart)
        user_cart.append(book_id)
        user_file.cart = str(user_cart)

        db_session.commit()


@app.route('/checkout')
def checkout():
    pass


@app.route('/library')
def library():
    pass


@app.route('/store')
def store():
    pass


@app.route('/order')
def order():
    pass


@app.route('/register_customer')
def register_customer():
    interests = ['Management', 'Leadership', 'Science', 'Mathematics', 'Programming', 'Architecture', 'Law',
                 'Commerce', 'Arts & Humanities', 'Nursery School', 'Primary School',
                 'Junior Secondary School', 'Senior Secondary School', 'University',
                 'Accounting', 'Automobiles', 'Aeronautics']

    if request.method == 'POST':
        page_no = request.form.get('page_no')

        name = request.form.get('name')
        email = request.form.get('email')
        phone = request.form.get('phone')
        birthday = request.form.get('birthday')
        password = request.form.get('password')
        c_password = request.form.get('c_password')

        st_address = request.form.get('address')
        city = request.form.get('city')
        interests = request.form.get('interests')
        postal_code = request.form.get('postal_code')

        if page_no == 1:
            if password == c_password:
                customer = Customer(name=name, email=email, birthday=birthday, password=password,
                                    phone=phone)
                db_session.add(customer)
                db_session.commit()

                session['user_email'] = email

            else:
                'flash'

        elif page_no == 2:
            user_file = get_user_file(email)  # .update({User.foo: User.foo + 1})
            user_file.address = {'street': st_address, 'city': city, 'postal/zip code': postal_code}
            user_file.interests = interests
            db_session.commit()


@app.route('/register_store')
def register_store():
    pass


@app.route('/aisle')
def aisle():
    aisle = request.get('aisle')


@app.route('/register_product')
def register_product():

    if request.method == 'POST':
        title = request.form.get('title')
        author = request.form.get('author')
        publisher = request.form.get('publisher')
        tags = request.form.get('tags')
        price = request.form.get('price')
        profit_margin = request.form.get('profit_margin')
        version = request.form.get('version')
        description = request.form.get('description')
        series = request.form.get('series')
        images = request.form.get('images')

        book = Book(title=title, author=author, publisher=publisher, tags=tags, price=price, version=version,
                    description=description, series=series, profit_margin=profit_margin, images=images)
        db_session.add(book)
        db_session.commit()
