import sys
sys.path.append('lib')

from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base


engine = create_engine('sqlite:///tmp/box.db', convert_unicode=True, echo=True)
metadata = MetaData(bind=engine)

customers = Table('customers', metadata,
                  Column('id', Integer, primary_key=True), Column('name', String), Column('birthday', String),
                  Column('email', String), Column('password', String), Column('address', String),
                  Column('phone', String), Column('interests', String), Column('received_emails', String),
                  Column('library', String), Column('cart', String), Column('store_credit', Integer),
                  Column('spare_dict', String))

products = Table('products', metadata,
                 Column('id', Integer, primary_key=True),
                 Column('title', String), Column('store', String), Column('tags', String),
                 Column('orders_customers', String), Column('orders_date', String), Column('orders_qty', String),
                 Column('interests_customers', String), Column('interests_date', String),
                 Column('interests_qty', String), Column('price', String), Column('profit_margin', String),
                 Column('ext_app', String), Column('series', String), Column('images', String),
                 Column('spare_dict', String))

stores = Table('stores', metadata,
               Column('id', Integer, primary_key=True),
               Column('name', String), Column('email_address', String), Column('city', String), Column('state', String),
               Column('ext_apps', String), Column('personalized', String), Column('stock', String),
               Column('spare_dict', String))

orders = Table('orders', metadata,
               Column('id', Integer, primary_key=True),
               Column('name', String), Column('customer_ids', String), Column('customer_fullname', String),
               Column('customer_email', String), Column('customer_address', String), Column('date', String),
               Column('books', String), Column('spare_dict', String))

db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()

metadata.create_all(bind=engine)
for _t in metadata.tables:
    print('Table: %s (exists)' % _t)
